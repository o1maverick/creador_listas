from django.contrib import admin
from django.urls import path
from Generador_Sub import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('main/',views.get_main),
    path('getting/info',views.get_data,name="ginf"),
    path('getting/minis/info',views.get_minis,name="gmin")
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
