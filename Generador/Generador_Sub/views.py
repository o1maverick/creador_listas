from django.shortcuts import render
from django.db import connection
# Create your views here.
from django.shortcuts import render
from django.http import JsonResponse
from django.core import serializers
from Generador_Sub.models import *
import json
def get_main(request):
    ejercito = Ejercito.objects.all()

    return render(request,"main.html",{'ejercitos':ejercito})


def get_data(request):
    if request.is_ajax and request.method == "GET":

        val_eje = request.GET.get("idejercito")

        idejercito = Ejercito.objects.all().filter(id=val_eje)

        regimienttos = Regimientos.objects.filter(idejercito=idejercito[0].id)

        unidades=Unidades.objects.filter(idejercito=idejercito[0].id)


        regimientos = serializers.serialize('json',regimienttos)
        unidad=serializers.serialize('json',unidades)

        data={}

        data['regimientos']=regimientos
        data['unidades']=unidad

        return JsonResponse(data,safe=False)

def get_minis(request):
    #LOS COMANDANTES NO ESTAN EN LA BASE DE DATOS DE MINIATURAS, HAY QUE CAMBIAR ESTA PARTE DEL CODIGO
    if request.is_ajax and request.method == "GET":
        if int(request.GET.get('cat')) > 0:
            nombreregimiento = request.GET.get('nomregiemiento')
            obj_regimiento = Regimientos.objects.filter(nombre=nombreregimiento)

            idregimiento = obj_regimiento[0].id

            obj_miniatruas = Miniaturas.objects.filter(idregimiento=idregimiento)

            obj_equiporegimiento = Equiporegimiento.objects.all().filter(idregimiento=idregimiento)

            data = {}
            data_eq={}
            for line in obj_equiporegimiento:
                obj_equipamiento = Equipos.objects.filter(id=line.idequipo.id)

                ser_equipamiento = serializers.serialize('json', obj_equipamiento)

                dic = {}
                for art in obj_equipamiento:

                    dic[0]=art.categoria_arma.categoria



                print(ser_equipamiento)


                data_eq[obj_equipamiento[0].id]=ser_equipamiento
                print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                print(data_eq)


            ser_miniaturas = serializers.serialize('json', obj_miniatruas)
            data['minis']=ser_miniaturas
            data['equ']=data_eq
            return JsonResponse(data, safe=False)
        else:
            nombreregimiento = request.GET.get('nomregiemiento')
            obj_regimiento = Regimientos.objects.filter(nombre=nombreregimiento)

            idregimiento = obj_regimiento[0].id
            #obj_miniatruas = Miniaturas.objects.filter(idregimiento=idregimiento)
            obj_equiporegimiento = Equiporegimiento.objects.all().filter(idregimiento=idregimiento)

            data = {}
            data_eq = {}
            for line in obj_equiporegimiento:
                print(line.idequipo.id)
                obj_equipamiento = Equipos.objects.filter(id=line.idequipo.id)
                ser_equipamiento = serializers.serialize('json', obj_equipamiento)
                data_eq[obj_equipamiento[0].id] = ser_equipamiento
            #ser_miniaturas = serializers.serialize('json', obj_miniatruas)

            #data['minis'] = ser_miniaturas
            data['equ'] = data_eq
            return JsonResponse(data, safe=False)

