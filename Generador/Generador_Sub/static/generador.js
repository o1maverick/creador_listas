



$('#army').change(function() {
    var idejercito = $("[name=selar]").val();
    $.ajax({
        type: 'GET',
        url: '/getting/info',
        dataType:'json',
        data: {
            'idejercito': idejercito,
        },
        success: function(response) {
            $("#comma > option").remove();
            $("#sel_hero > option").remove();
            $("#sel_base > option").remove();
            $("#sel_special > option").remove();
            $("#sel_sing > option").remove();
            $("#fhero").remove();
            $("#fspecial").remove();
            $("#sing").remove();
            $("#second_base > div").remove();
            $("#second_base_com > div").remove();
            $("#second_hero > div").remove();
            $("#second_special > div").remove();
            $("#second_sing > div").remove();
            var list = jQuery('[id^=prin]');
            list.remove();

            $("#comma").append('<option value="0">selecciona regimiento</option>');
            $("#sel_hero").append('<option value="0">selecciona regimiento</option>');
            $("#sel_base").append('<option value="0">selecciona regimiento</option>');
            $("#sel_special").append('<option value="0">selecciona regimiento</option>');
            $("#sel_sing").append('<option value="0">selecciona regimiento</option>');

            $.each(JSON.parse(response["regimientos"]), function(id,values){
                console.log(values)
                if ((values.fields.idunidad == 4 ) || (values.fields.idunidad == 9) || (values.fields.idunidad==11)){

                    $("#comma").append('<option value="'+values.fields.nombre+'">'+ values.fields.nombre +'</option>');
                    $("#quantityt > span").html("0-1");

                }

                if ((values.fields.idunidad == 5 ) || (values.fields.idunidad == 10) || (values.fields.idunidad==12)){

                    $("#sel_hero").append('<option value="'+values.fields.nombre+'">'+ values.fields.nombre +'</option>');
                    $("#quantity_herot > span").html("0-4");


                }

                if ((values.fields.idunidad == 1 ) || (values.fields.idunidad == 6) || (values.fields.idunidad==13)){
                    if (values.fields.idunidad == 1 ) {
                        $("#quantity_baset > span").html("+2");
                    }else{
                        $("#quantity_baset > span").html("+3");
                    }

                    $("#sel_base").append('<option value="'+values.fields.nombre+'">'+ values.fields.nombre +'</option>');

                }

                 if ((values.fields.idunidad == 2 ) || (values.fields.idunidad == 7) || (values.fields.idunidad==14)){

                    if (values.fields.idunidad == 2 ) {
                        $("#quantity_specialt > span").html("0-6");
                    }else{
                        $("#quantity_specialt > span").html("0-4");
                    }
                    $("#sel_special").append('<option value="'+values.fields.nombre+'">'+ values.fields.nombre +'</option>');
                }
                if ((values.fields.idunidad == 3 ) || (values.fields.idunidad == 8) || (values.fields.idunidad==15)){

                    if (values.fields.idunidad == 3 ) {
                        $("#quantity_singt > span").html("0-4");
                    }else{
                        $("#quantity_singt > span").html("0-2");
                    }
                    $("#sel_sing").append('<option value="'+ values.fields.nombre +'">'+ values.fields.nombre +'</option>');
                }
            });
            $.each(JSON.parse(response["unidades"]),function(id,values){
                var idelemento = '';
                var idbutton = '';
                var idlabelr='';
                var maximo = values.fields.maximo;
                var minimo = 0;
                if(values.fields.categoria=="comandantes"){
                    idelemento='#comma';
                    idbutton='btn_comm';

                    var string = '<div id="prin" class="container-fluid"></div>';
                    $(string).insertAfter(idelemento);
                    var string2 = '<div id="'+idf+'" class="container-fluid"><br><form  action=""></form></div><div class="container-fluid" id="second_base_com"></div>';
                    $(string2).insertAfter("#prin");

                }else{
                if(values.fields.categoria=="heroes"){
                    idelemento='#sel_hero';
                    idlabelr='quantity_heros';
                    idbutton='btn_hero';
                    minimo = 0;
                    idf = 'fhero';
                    sb = 'second_hero';
                }
                if(values.fields.categoria=="basicas"){
                    idelemento='#sel_base';
                    idlabelr='quantity_base';
                    idbutton='btn_base';
                    minimo = maximo;
                    maximo=20;
                    idf = 'prin_base';
                    sb = 'second_base';
                }
                if(values.fields.categoria=="especiales"){
                    idelemento='#sel_special';
                    idlabelr='quantity_special';
                    idbutton='btn_special';
                    idf = 'fspecial';
                    sb = 'second_special';
                }
                if(values.fields.categoria=="singulares"){
                    idelemento='#sel_sing';
                    idlabelr='quantity_sing';
                    idbutton='btn_sing';
                    idf = 'sing';
                    sb = 'second_sing';
                }
                var string = '<div id="'+idf+'" class="container-fluid"><br><form  action=""></form></div><div class="container-fluid" id="'+sb+'"></div>';
               $(string).insertAfter(idelemento);
                }
            });
        }
    });
});

$('body').on("change","#comma",function() {
    var cat=0;
    var nombrereg = $('#comma').val();
    var quantity = 1;
    $("#second_base_com > div").remove();

    if(quantity<1){
        alert("debe de haber al menos 1 regimiento");
    }else{
        $.ajax({
            type: 'GET',
            url: '/getting/minis/info',
            dataType:'json',
            data: {
                'nomregiemiento': nombrereg,
                'cat': cat,
            },
            success: function(response) {


                var content_arm=response["equ"];
                var contenido = '';

                contenido = '<div id="fir_con" class="container-fluid"></div><div id="sec_con" class="container-fluid"></div><div id="ter_con_comma" class="container-fluid"><div class="alert alert-light" role="alert">Armamento</div></div>';
                $('#second_base_com').append(contenido);
                if(Object.keys(content_arm).length>0){
                         $.each(content_arm, function(id,values){
                            data = JSON.parse(values)[0].fields;
                            contenido ='<div class="container-fluid"><div class="form-check">'+
                                  '<input class="form-check-input" type="checkbox" value="'+data.nombre+'" id="flexCheckDefault">'+
                                  '<label class="form-check-label" for="flexCheckDefault">'+
                                   data.nombre+
                                  '</label></div></div>';
                            $('#ter_con_comma').append(contenido);
                        });
                        contenido='<div class="container-fluid"><button type="button" class="btn btn-primary">Añadir</button></div>';
                        $('#ter_con_comma').append(contenido);
                   }else{
                        contenido ='<div class="container-fluid">Sin Equipo</div><button type="button" class="btn btn-primary">Añadir</button>';
                        $('#ter_con_comma').append(contenido);
                    }

            }
        });
    }
});

$('body').on("change","#sel_hero",function() {
    var cat=0;
    var nombrereg = $('#sel_hero').val();
    var quantity = 1;
    $("#second_hero > div").remove();

    if(quantity<1){
        alert("debe de haber al menos 1 regimiento");
    }else{
        $.ajax({
            type: 'GET',
            url: '/getting/minis/info',
            dataType:'json',
            data: {
                'nomregiemiento': nombrereg,
                'cat': cat,
            },
            success: function(response) {


                var content_arm=response["equ"];
                var contenido = '';

                contenido = '<div id="fir_con" class="container-fluid"></div><div id="sec_con" class="container-fluid"></div><div id="ter_con_hero" class="container-fluid"><div class="alert alert-light" role="alert">Armamento</div></div>';
                $('#second_hero').append(contenido);
                if(Object.keys(content_arm).length>0){
                         $.each(content_arm, function(id,values){
                            data = JSON.parse(values)[0].fields;
                            contenido ='<div class="container-fluid"><div class="form-check">'+
                                  '<input class="form-check-input" type="checkbox" value="'+data.nombre+'" id="flexCheckDefault">'+
                                  '<label class="form-check-label" for="flexCheckDefault">'+
                                   data.nombre+
                                  '</label></div></div>';
                            $('#ter_con_hero').append(contenido);
                        });
                        contenido='<div class="container-fluid"><button type="button" class="btn btn-primary">Añadir</button></div>';
                        $('#ter_con_hero').append(contenido);
                   }else{
                        contenido ='<div class="container-fluid">Sin Equipo</div><button type="button" class="btn btn-primary">Añadir</button>';
                        $('#ter_con_hero').append(contenido);
                    }

            }
        });
    }
});

$('body').on("change","#sel_base",function() {
    var nombrereg = $('#sel_base').val();
    var quantity = $('#quantity_base').val();
    $("#second_base > div").remove();

    if(quantity<1){
        alert("debe de haber al menos 1 regimiento");
    }else{
        $.ajax({
            type: 'GET',
            url: '/getting/minis/info',
            dataType:'json',
            data: {
                'nomregiemiento': nombrereg,
                'cat': 1,
            },
            success: function(response) {
                var content_mini=JSON.parse(response["minis"]);
                var content_arm=response["equ"];
                var contenido = '';
                $.each(content_mini, function(id,values){
                    contenido = '<div id="fir_con" class="container-fluid"><div class="alert alert-light" role="alert">Tamaño unidad</div></div>'+'<div id="sec_con" class="container-fluid"><form id="" action=""><label for=""></label><input type="number" id="baseq" name="quantity" min="'+values.fields.minimo+'" max="" step="1" value="'+values.fields.minimo+'" cant="'+values.fields.puntos+'"></form><span id="resbase" value="0"></span>pts</div>'+
                    '<div id="ter_con" class="container-fluid"><div class="alert alert-light" role="alert">Armamento</div></div>';
                    $('#second_base').append(contenido);
                    calcular_puntos();
                    if(Object.keys(content_arm).length>0){
                         $.each(content_arm, function(id,values){
                            data = JSON.parse(values)[0].fields;

                            console.log(id['categoria']);
                            if (data.categoria == 1){
                                contenido ='<div class="container-fluid"><div class="form-check">'+
                                  '<input class="form-check-input" type="checkbox" value="'+data.nombre+'" id="flexCheckDefault">'+
                                  '<label class="form-check-label" for="flexCheckDefault">'+
                                   data.nombre+'</label></div></div>';
                            }else if(data.categoria == 2){
                                contenido ='<div class="container-fluid"><div class="form-check">'+
                                '<input class="form-check-input" type="checkbox" value="'+data.nombre+'" id="flexCheckDefault">'+
                                '<label class="form-check-label" for="flexCheckDefault">'+
                                 data.nombre+'</label></div></div>';
                                $('#ter_con').append(contenido);
                            }else if(data.categoria == 3){
                                contenido ='<div class="container-fluid"><div class="form-check">'+
                                '<input class="form-check-input" type="checkbox" value="'+data.nombre+'" id="flexCheckDefault">'+
                                '<label class="form-check-label" for="flexCheckDefault">'+
                                 data.nombre+'</label></div></div>';
                                $('#ter_con').append(contenido);
                            }

                        });
                        contenido='<div class="container-fluid"><button type="button" id="addbase" class="btn btn-primary">Añadir</button></div>';
                        $('#ter_con').append(contenido);
                    }else{
                        contenido ='<div class="container-fluid">Sin Equipo</div><button type="button" id="addbase" class="btn btn-primary">Añadir</button>';
                        $('#ter_con').append(contenido);
                    }
                });
            }
        });
    }
});

$('body').on("change","#sel_special",function() {
    var nombrereg = $('#sel_special').val();
    var quantity = $('#quantity_base').val();
    $("#second_special > div").remove();
    $("#second_special > button").remove();

    if(quantity<1){
        alert("debe de haber al menos 1 regimiento");
    }else{
        $.ajax({
            type: 'GET',
            url: '/getting/minis/info',
            dataType:'json',
            data: {
                'nomregiemiento': nombrereg,
                'cat': 1,
            },
            success: function(response) {
                var content_mini=JSON.parse(response["minis"]);
                var content_arm=response["equ"];
                var contenido = '';

                if(content_mini.length>0){
                $.each(content_mini, function(id,values){
                    contenido = '<div id="fir_con" class="container-fluid"><div class="alert alert-light" role="alert">Tamaño unidad</div></div>'+'<div id="sec_con" class="container-fluid"><form id="" action=""><label for=""></label><input type="number" id="specialq" name="quantity" min="'+values.fields.minimo+'" max="" step="1" value="'+values.fields.minimo+'"></form></div>'+
                    '<div id="ter_con_special" class="container-fluid"><div class="alert alert-light" role="alert">Armamento</div></div>';
                    $('#second_special').append(contenido);

                        if(Object.keys(content_arm).length>0){
                             $.each(content_arm, function(id,values){
                                data = JSON.parse(values)[0].fields;

                                contenido ='<div class="container-fluid"><div class="form-check">'+
                                      '<input class="form-check-input" type="checkbox" value="'+data.nombre+'" id="flexCheckDefault">'+
                                      '<label class="form-check-label" for="flexCheckDefault">'+
                                       data.nombre+
                                      '</label></div></div>';
                                $('#ter_con_special').append(contenido);
                            });
                            contenido='<div class="container-fluid"><button type="button" class="btn btn-primary">Añadir</button></div>';
                            $('#ter_con_special').append(contenido);
                        }else{
                            contenido ='<div class="container-fluid">Sin Equipo</div><button type="button" class="btn btn-primary">Añadir</button>';
                            $('#ter_con_special').append(contenido);
                        }

                });
                }else{
                            contenido ='<div class="container-fluid">Sin Equipo</div><button type="button" class="btn btn-primary">Añadir</button>';
                            $('#second_special').append(contenido);
                        }
            }
        });
    }
});

$('body').on("change","#sel_sing",function() {
    var nombrereg = $('#sel_sing').val();
    var quantity = $('#quantity_base').val();
    $("#second_sing > div").remove();
    $("#second_sing > button").remove();

    if(quantity<1){
        alert("debe de haber al menos 1 regimiento");
    }else{
        $.ajax({
            type: 'GET',
            url: '/getting/minis/info',
            dataType:'json',
            data: {
                'nomregiemiento': nombrereg,
                'cat': 1,
            },
            success: function(response) {
                var content_mini=JSON.parse(response["minis"]);
                var content_arm=response["equ"];
                var contenido = '';

                if(content_mini.length>0){
                $.each(content_mini, function(id,values){
                    contenido = '<div id="fir_con" class="container-fluid"><div class="alert alert-light" role="alert">Tamaño unidad</div></div>'+'<div id="sec_con" class="container-fluid"><form id="" action=""><label for=""></label><input type="number" id="singq" name="quantity" min="'+values.fields.minimo+'" max="" step="1" value="'+values.fields.minimo+'"></form></div>'+
                    '<div id="ter_con_sing" class="container-fluid"><div class="alert alert-light" role="alert">Armamento</div></div>';
                    $('#second_sing').append(contenido);

                        if(Object.keys(content_arm).length>0){
                             $.each(content_arm, function(id,values){
                                data = JSON.parse(values)[0].fields;
                                contenido ='<div class="container-fluid"><div class="form-check">'+
                                      '<input class="form-check-input" type="checkbox" value="'+data.nombre+'" id="flexCheckDefault">'+
                                      '<label class="form-check-label" for="flexCheckDefault">'+
                                       data.nombre+
                                      '</label></div></div>';
                                $('#ter_con_sing').append(contenido);
                            });
                            contenido='<div class="container-fluid"><button type="button" class="btn btn-primary">Añadir</button></div>';
                            $('#ter_con_sing').append(contenido);
                        }else{
                            contenido ='<div class="container-fluid">Sin Equipo</div><button type="button" class="btn btn-primary">Añadir</button>';
                            $('#ter_con_sing').append(contenido);
                        }

                });
                }else{
                            contenido ='<div class="container-fluid">Sin Equipo</div><button type="button" class="btn btn-primary">Añadir</button>';
                            $('#second_sing').append(contenido);
                        }
            }
        });
    }
});

$('body').on("change","#baseq",function() {
    calcular_puntos();
});


function calcular_puntos(){
    var puntos_individual = $('#baseq').attr('cant');
    var numero_minis = $('#baseq').val();

    var puntos = puntos_individual*numero_minis;

    $('#resbase').text(puntos);
}


$('#btn_comm').click(function() {
    var nombrereg = $('#comma').val();
    console.log(nombrereg)
    $.ajax({
        type: 'GET',
        url: '/getting/minis/info',
        dataType:'json',
        data: {
            'nomregiemiento': nombrereg,
        },
        success: function(response) {

        }
    });
});

$('#btn_hero').click(function() {
    var nombrereg = $('#sel_hero').val();
    console.log(nombrereg)
    $.ajax({
        type: 'GET',
        url: '/getting/minis/info',
        dataType:'json',
        data: {
            'nomregiemiento': nombrereg,
        },
        success: function(response) {

        }
    });
});

$('body').on("click","#addbase",function() {
    var nombrereg = $('#baseq').val();
    alert(nombrereg);
});

$('#btn_special').click(function() {
    var nombrereg = $('#sel_special').val();
    console.log(nombrereg)
    $.ajax({
        type: 'GET',
        url: '/getting/minis/info',
        dataType:'json',
        data: {
            'nomregiemiento': nombrereg,
        },
        success: function(response) {

        }
    });
});

$('#btn_sing').click(function() {
    var nombrereg = $('#sel_sing').val();
    console.log(nombrereg)
    $.ajax({
        type: 'GET',
        url: '/getting/minis/info',
        dataType:'json',
        data: {
            'nomregiemiento': nombrereg,
        },
        success: function(response) {

        }
    });
});