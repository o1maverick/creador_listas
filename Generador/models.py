# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Campeon(models.Model):
    id = models.IntegerField(primary_key=True)
    puntos = models.IntegerField()
    puntosequipo = models.IntegerField(blank=True, null=True)
    idregimiento = models.ForeignKey('Regimientos', models.DO_NOTHING, db_column='idregimiento')

    class Meta:
        managed = False
        db_table = 'campeon'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Ejercito(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'ejercito'


class Equiporegimiento(models.Model):
    id = models.IntegerField(primary_key=True)
    idequipo = models.ForeignKey('Equipos', models.DO_NOTHING, db_column='idequipo')
    idregimiento = models.ForeignKey('Regimientos', models.DO_NOTHING, db_column='idregimiento')

    class Meta:
        managed = False
        db_table = 'equiporegimiento'


class Equipos(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)
    puntos = models.IntegerField()
    idejercito = models.ForeignKey(Ejercito, models.DO_NOTHING, db_column='idejercito', blank=True, null=True)
    categoria_arma = models.ForeignKey('TipoEquipo', models.DO_NOTHING, db_column='categoria_arma')
    cat = models.CharField(max_length=100, blank=True, null=True)
    mago = models.IntegerField(blank=True, null=True)
    pie = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'equipos'


class Miniaturas(models.Model):
    id = models.IntegerField(primary_key=True)
    maximo = models.IntegerField(blank=True, null=True)
    minimo = models.IntegerField(blank=True, null=True)
    puntos = models.IntegerField()
    idregimiento = models.ForeignKey('Regimientos', models.DO_NOTHING, db_column='idregimiento')

    class Meta:
        managed = False
        db_table = 'miniaturas'


class Musico(models.Model):
    id = models.IntegerField()
    puntos = models.IntegerField()
    idregimiento = models.ForeignKey('Regimientos', models.DO_NOTHING, db_column='idregimiento')

    class Meta:
        managed = False
        db_table = 'musico'


class Portaestandarte(models.Model):
    id = models.IntegerField()
    puntos = models.IntegerField()
    puntosequipo = models.IntegerField(blank=True, null=True)
    idregimiento = models.ForeignKey('Regimientos', models.DO_NOTHING, db_column='idregimiento')

    class Meta:
        managed = False
        db_table = 'portaestandarte'


class Regimientos(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)
    idunidad = models.ForeignKey('Unidades', models.DO_NOTHING, db_column='idunidad')
    puntos = models.IntegerField(blank=True, null=True)
    objetos_magicos = models.IntegerField(blank=True, null=True)
    idejercito = models.ForeignKey(Ejercito, models.DO_NOTHING, db_column='idejercito', blank=True, null=True)
    tipo = models.CharField(max_length=100, blank=True, null=True)
    pie = models.IntegerField(blank=True, null=True)
    mago = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'regimientos'


class TipoEquipo(models.Model):
    id = models.IntegerField(primary_key=True)
    tipo = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'tipo_equipo'


class Unidades(models.Model):
    id = models.IntegerField(primary_key=True)
    categoria = models.CharField(max_length=100)
    maximo = models.IntegerField()
    puntospartida = models.IntegerField()
    idejercito = models.ForeignKey(Ejercito, models.DO_NOTHING, db_column='idejercito')

    class Meta:
        managed = False
        db_table = 'unidades'


class Unionejercitoregimiento(models.Model):
    id = models.IntegerField(primary_key=True)
    idejercito = models.ForeignKey(Ejercito, models.DO_NOTHING, db_column='idejercito')
    idregimiento = models.ForeignKey(Regimientos, models.DO_NOTHING, db_column='idregimiento')

    class Meta:
        managed = False
        db_table = 'unionejercitoregimiento'
