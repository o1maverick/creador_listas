-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-01-2021 a las 14:57:16
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `warhammer`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campeon`
--

CREATE TABLE `campeon` (
  `id` int(11) NOT NULL,
  `puntos` int(11) NOT NULL,
  `puntosequipo` int(11) DEFAULT NULL,
  `idregimiento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `campeon`
--

INSERT INTO `campeon` (`id`, `puntos`, `puntosequipo`, `idregimiento`) VALUES
(1, 10, NULL, 12),
(2, 10, NULL, 13),
(3, 10, NULL, 14),
(4, 12, 25, 15),
(5, 12, 25, 16),
(6, 12, 25, 17),
(7, 16, NULL, 18),
(8, 20, 25, 19),
(9, 7, NULL, 20),
(10, 12, NULL, 21),
(11, 15, NULL, 49),
(12, 15, NULL, 50),
(13, 15, NULL, 51),
(14, 8, NULL, 52),
(15, 12, NULL, 53),
(16, 12, NULL, 54),
(17, 8, NULL, 55),
(18, 20, NULL, 57),
(19, 17, NULL, 58),
(20, 17, NULL, 59),
(21, 12, NULL, 84),
(22, 8, NULL, 85),
(23, 12, NULL, 87),
(24, 20, 25, 88),
(25, 20, NULL, 91),
(26, 20, NULL, 92),
(27, 20, NULL, 94);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ejercito`
--

CREATE TABLE `ejercito` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ejercito`
--

INSERT INTO `ejercito` (`id`, `nombre`) VALUES
(1, 'altos elfos'),
(2, 'orcos y goblins'),
(3, 'guerreros del caos'),
(4, 'enanos'),
(5, 'hombres lagartos'),
(6, 'skavens');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equiporegimiento`
--

CREATE TABLE `equiporegimiento` (
  `id` int(11) NOT NULL,
  `idequipo` int(11) NOT NULL,
  `idregimiento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equiporegimiento`
--

INSERT INTO `equiporegimiento` (`id`, `idequipo`, `idregimiento`) VALUES
(1, 2, 5),
(2, 1, 5),
(3, 12, 5),
(4, 4, 5),
(5, 6, 5),
(6, 3, 5),
(7, 5, 5),
(8, 9, 5),
(9, 7, 5),
(10, 8, 5),
(11, 17, 5),
(12, 10, 5),
(13, 11, 5),
(14, 16, 5),
(15, 15, 5),
(16, 14, 5),
(17, 18, 5),
(18, 13, 5),
(19, 10, 6),
(20, 11, 6),
(21, 12, 6),
(22, 13, 6),
(23, 14, 6),
(24, 15, 6),
(25, 17, 6),
(26, 19, 6),
(27, 20, 8),
(28, 1, 9),
(29, 2, 9),
(30, 3, 9),
(31, 4, 9),
(32, 5, 9),
(33, 10, 9),
(34, 11, 9),
(35, 12, 9),
(36, 17, 9),
(37, 7, 9),
(38, 8, 9),
(39, 9, 9),
(40, 21, 9),
(41, 6, 9),
(42, 22, 10),
(43, 10, 10),
(44, 11, 10),
(45, 17, 10),
(46, 22, 11),
(47, 24, 12),
(48, 23, 14),
(49, 21, 18),
(50, 25, 20),
(51, 26, 20),
(52, 92, 31),
(53, 93, 31),
(54, 94, 31),
(55, 95, 31),
(56, 97, 31),
(57, 98, 31),
(58, 99, 31),
(59, 100, 31),
(60, 92, 32),
(61, 93, 32),
(62, 94, 32),
(63, 95, 32),
(64, 97, 32),
(65, 98, 32),
(66, 99, 32),
(67, 100, 32),
(68, 96, 33),
(69, 97, 33),
(70, 98, 33),
(71, 99, 33),
(72, 100, 33),
(73, 98, 34),
(74, 99, 34),
(75, 100, 34),
(76, 19, 34),
(77, 98, 35),
(78, 99, 35),
(79, 100, 35),
(80, 19, 35),
(81, 92, 36),
(82, 5, 36),
(83, 2, 36),
(84, 95, 36),
(85, 97, 36),
(86, 101, 36),
(87, 102, 36),
(88, 103, 36),
(89, 104, 36),
(90, 92, 37),
(91, 5, 37),
(92, 2, 37),
(93, 95, 37),
(94, 97, 37),
(95, 101, 37),
(96, 102, 37),
(97, 103, 37),
(98, 104, 37),
(99, 101, 38),
(100, 103, 38),
(101, 19, 38),
(102, 101, 39),
(103, 103, 39),
(104, 19, 39),
(105, 106, 40),
(106, 21, 40),
(107, 107, 40),
(108, 100, 40),
(109, 105, 40),
(110, 106, 41),
(111, 21, 41),
(112, 107, 41),
(113, 100, 41),
(114, 105, 41),
(115, 108, 42),
(116, 21, 42),
(117, 107, 42),
(118, 100, 42),
(119, 105, 42),
(120, 107, 43),
(121, 22, 43),
(122, 107, 44),
(123, 22, 44),
(124, 109, 45),
(125, 102, 45),
(126, 103, 45),
(127, 105, 45),
(128, 104, 46),
(129, 105, 46),
(130, 109, 47),
(131, 103, 47),
(132, 22, 47),
(133, 22, 48),
(134, 110, 49),
(135, 111, 49),
(136, 23, 49),
(137, 112, 49),
(138, 113, 49),
(139, 110, 51),
(140, 111, 51),
(141, 114, 51),
(142, 23, 51),
(143, 112, 51),
(144, 111, 52),
(145, 115, 52),
(146, 23, 52),
(147, 111, 53),
(148, 115, 53),
(149, 23, 53),
(150, 115, 54),
(151, 111, 55),
(152, 116, 55),
(153, 117, 55),
(154, 118, 55),
(155, 23, 57),
(156, 112, 58),
(157, 119, 59),
(158, 21, 59),
(159, 112, 59),
(160, 120, 60),
(161, 121, 63),
(162, 122, 63),
(163, 123, 65),
(164, 123, 64),
(165, 124, 66),
(166, 3, 76),
(167, 175, 76),
(168, 176, 76),
(169, 177, 76),
(170, 178, 76),
(171, 179, 76),
(172, 180, 76),
(173, 181, 76),
(174, 182, 76),
(175, 183, 76),
(176, 184, 76),
(177, 185, 76),
(178, 186, 76),
(179, 187, 76),
(180, 188, 76),
(181, 189, 76),
(182, 190, 76),
(183, 191, 76),
(184, 192, 76),
(185, 19, 77),
(186, 180, 77),
(187, 193, 77),
(188, 182, 77),
(189, 183, 77),
(190, 184, 77),
(191, 185, 77),
(192, 186, 77),
(193, 187, 77),
(194, 188, 77),
(195, 189, 77),
(196, 191, 77),
(197, 192, 77),
(198, 194, 78),
(199, 195, 78),
(200, 196, 78),
(201, 182, 78),
(202, 197, 78),
(203, 22, 82),
(204, 180, 82),
(205, 193, 82),
(206, 182, 82),
(207, 198, 82),
(208, 199, 82),
(209, 187, 82),
(210, 188, 82),
(211, 189, 82),
(212, 191, 82),
(213, 185, 82),
(214, 201, 83),
(215, 202, 83),
(216, 203, 83),
(217, 205, 83),
(218, 179, 83),
(219, 180, 83),
(220, 193, 83),
(221, 182, 83),
(222, 198, 83),
(223, 199, 83),
(224, 187, 83),
(225, 188, 83),
(226, 189, 83),
(227, 190, 83),
(228, 191, 83),
(229, 192, 83),
(230, 206, 83),
(231, 207, 78),
(232, 208, 85),
(233, 195, 85),
(234, 193, 85),
(235, 204, 85),
(236, 23, 85),
(237, 212, 85),
(238, 213, 85),
(239, 24, 85),
(240, 214, 86),
(241, 215, 86),
(242, 208, 87),
(243, 195, 87),
(244, 216, 87),
(245, 204, 87),
(246, 23, 87),
(247, 24, 87),
(248, 217, 87),
(249, 111, 87),
(250, 218, 87),
(251, 219, 87),
(252, 208, 88),
(253, 195, 88),
(254, 216, 88),
(255, 204, 88),
(256, 23, 88),
(257, 209, 88),
(258, 210, 88),
(259, 211, 88),
(260, 208, 90),
(261, 195, 90),
(262, 216, 90),
(263, 204, 90),
(264, 208, 91),
(265, 195, 91),
(266, 216, 91),
(267, 204, 91),
(268, 220, 91),
(269, 221, 91),
(270, 222, 91),
(273, 208, 94),
(274, 195, 94),
(275, 216, 94),
(276, 204, 94),
(277, 224, 94),
(280, 175, 92),
(281, 223, 92),
(288, 225, 95),
(289, 195, 95),
(290, 181, 95),
(291, 226, 95);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `puntos` int(11) NOT NULL,
  `idejercito` int(11) DEFAULT NULL,
  `categoria_arma` int(11) NOT NULL,
  `cat` varchar(100) DEFAULT NULL,
  `mago` int(1) DEFAULT NULL,
  `pie` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `nombre`, `puntos`, `idejercito`, `categoria_arma`, `cat`, `mago`, `pie`) VALUES
(1, 'lanza de caballeria', 6, NULL, 1, NULL, NULL, NULL),
(2, 'lanza', 3, NULL, 1, NULL, NULL, NULL),
(3, 'arma a dos manos', 12, NULL, 1, NULL, NULL, NULL),
(4, 'alabarda', 6, NULL, 1, NULL, NULL, NULL),
(5, 'arma de mano adicional', 6, NULL, 1, NULL, NULL, NULL),
(6, 'arco largo', 10, NULL, 1, NULL, NULL, NULL),
(7, 'armadura ligera', 3, NULL, 2, NULL, NULL, NULL),
(8, 'armadura pesada', 6, NULL, 2, NULL, NULL, NULL),
(9, 'armadura de dragon', 9, 1, 2, NULL, NULL, NULL),
(10, 'corcel elfico', 18, 1, 3, NULL, NULL, NULL),
(11, 'corcel elfico con barda', 24, 1, 3, NULL, NULL, NULL),
(12, 'aguila gigante', 50, 1, 3, NULL, NULL, NULL),
(13, 'grifo', 200, 1, 3, NULL, NULL, NULL),
(14, 'dragon solar', 230, 1, 3, NULL, NULL, NULL),
(15, 'dragon lunar', 300, 1, 3, NULL, NULL, NULL),
(16, 'dragon estelar', 370, 1, 3, NULL, NULL, NULL),
(17, 'carro de tiranoc', 85, 1, 3, NULL, NULL, NULL),
(18, 'escudo', 3, NULL, 2, NULL, NULL, NULL),
(19, 'hechicero nv4', 35, NULL, 11, NULL, NULL, NULL),
(20, 'carro de leones', 130, 1, 3, NULL, NULL, NULL),
(21, 'escudo (2)', 2, NULL, 2, NULL, NULL, NULL),
(22, 'hechicero nv2', 35, NULL, 12, NULL, NULL, NULL),
(23, 'escudo (1)', 1, NULL, 2, NULL, NULL, NULL),
(24, 'armadura ligera (1)', 1, NULL, 2, NULL, NULL, NULL),
(25, 'arco(4)', 4, NULL, 1, NULL, NULL, NULL),
(26, 'cambio de lanza por arco', 2, 1, 10, NULL, NULL, NULL),
(27, 'espada veloz', 25, 1, 10, NULL, NULL, NULL),
(28, 'espada de batalla', 20, 1, 10, NULL, NULL, NULL),
(29, 'espada del poder', 15, 1, 10, NULL, NULL, NULL),
(30, 'mordisco de acero', 10, 1, 10, NULL, NULL, NULL),
(31, 'escudo hechizado', 10, 1, 9, NULL, NULL, NULL),
(32, 'talisman de proteccion', 20, 1, 8, NULL, NULL, NULL),
(33, 'pergamino de dispersion', 20, 1, 6, NULL, NULL, NULL),
(34, 'piedra de energia', 20, 1, 6, NULL, NULL, NULL),
(35, 'baculo de hechicero', 40, 1, 6, NULL, NULL, NULL),
(36, 'estandarte de guerra', 20, 1, 7, NULL, NULL, NULL),
(37, 'espada de oro fulgurante', 60, 1, 10, NULL, NULL, NULL),
(38, 'arco de ellyrion', 40, 1, 10, NULL, NULL, NULL),
(39, 'arco del navegante', 60, 1, 10, NULL, NULL, NULL),
(40, 'espada deslumbrante', 40, 1, 10, NULL, NULL, 1),
(41, 'espada de hoeth', 60, 1, 10, NULL, NULL, NULL),
(42, 'espada de oro marino', 40, 1, 10, NULL, NULL, NULL),
(43, 'lanza de caballeria estelar', 40, 1, 10, NULL, NULL, 0),
(44, 'azote del mal', 25, 1, 10, NULL, NULL, NULL),
(45, 'armadura de proteccion', 45, 1, 9, NULL, NULL, NULL),
(46, 'armadura sombria', 25, 1, 9, NULL, NULL, NULL),
(47, 'armadura de caledor', 25, 1, 9, NULL, NULL, NULL),
(48, 'escudo aureo', 35, 1, 9, NULL, NULL, NULL),
(49, 'yelmo del destino', 25, 1, 9, NULL, NULL, NULL),
(50, 'armadura estelar', 30, 1, 9, NULL, NULL, 1),
(51, 'esudo de escamas de dragon', 20, 1, 9, NULL, NULL, NULL),
(52, 'armadura de los heroes', 30, 1, 9, NULL, NULL, NULL),
(53, 'mascara de merlod', 10, 1, 9, NULL, NULL, 1),
(54, 'guanteletes de temakador', 30, 1, 9, NULL, NULL, NULL),
(55, 'brazaletes de defensa', 55, 1, 8, NULL, NULL, NULL),
(56, 'incienso sagrado', 30, 1, 8, NULL, NULL, NULL),
(57, 'corona dorada de atrazar', 40, 1, 8, NULL, NULL, NULL),
(58, 'capa del señor del conocimiento', 40, 1, 8, NULL, NULL, NULL),
(59, 'talisman de saphery', 35, 1, 8, NULL, NULL, NULL),
(60, 'la llama del fenix', 25, 1, 8, NULL, NULL, NULL),
(61, 'amuleto del fuego', 20, 1, 8, NULL, NULL, NULL),
(62, 'estandarte de batalla', 80, 1, 7, NULL, NULL, NULL),
(63, 'estandarte de la templanza', 45, 1, 7, NULL, NULL, NULL),
(64, 'estandarte del dragon', 60, 1, 7, NULL, NULL, NULL),
(65, 'estandarte de saphery', 50, 1, 7, NULL, NULL, NULL),
(66, 'estandarte de proteccion arcana', 25, 1, 7, NULL, NULL, NULL),
(67, 'estandarte de leon', 25, 1, 7, NULL, NULL, NULL),
(68, 'estandarte de ellyrion', 15, 1, 7, NULL, NULL, NULL),
(69, 'libro de hoeth', 100, 1, 6, NULL, NULL, NULL),
(70, 'fragmento del vortice', 75, 1, 6, NULL, NULL, NULL),
(71, 'cristal de las annulii', 40, 1, 6, NULL, NULL, NULL),
(72, 'baculo de arbol estelar', 40, 1, 6, NULL, NULL, NULL),
(73, 'baculo del vidente de saphery', 30, 1, 6, NULL, NULL, NULL),
(74, 'colgante del engano', 30, 1, 6, NULL, NULL, NULL),
(75, 'gema del fuego solar', 25, 1, 6, NULL, NULL, NULL),
(76, 'anillo de corin', 20, 1, 6, NULL, NULL, NULL),
(77, 'baculo de la solidez', 20, 1, 6, NULL, NULL, NULL),
(78, 'rubi crepuscular', 15, 1, 6, NULL, NULL, NULL),
(79, 'vara de plata', 10, 1, 6, NULL, NULL, NULL),
(80, 'piedra de la anulacion', 100, 1, 5, NULL, NULL, NULL),
(81, 'pocion de curacion', 50, 1, 5, NULL, NULL, NULL),
(82, 'gema radiante de hoeth', 45, 1, 5, NULL, NULL, NULL),
(83, 'toga de folariath', 45, 1, 5, NULL, NULL, 1),
(84, 'anillo de la ira', 40, 1, 5, NULL, NULL, NULL),
(85, 'capa de las barbas', 35, 1, 5, NULL, NULL, NULL),
(86, 'colgante de la venganza', 35, 1, 5, NULL, NULL, NULL),
(87, 'cuerno del dragon', 25, 1, 5, NULL, NULL, NULL),
(88, 'amuleto de clarividencia', 25, 1, 5, NULL, NULL, NULL),
(89, 'amuleto de la luz', 15, 1, 5, NULL, NULL, NULL),
(90, 'gema del coraje', 10, 1, 5, NULL, NULL, NULL),
(91, 'talisman de loec', 10, 1, 5, NULL, NULL, NULL),
(92, 'hacha a dos manos(6)', 6, 2, 1, NULL, NULL, NULL),
(93, 'rebanadora adicional(6)', 6, 2, 1, NULL, NULL, NULL),
(94, 'lanza(3)', 3, 2, 1, NULL, NULL, NULL),
(95, 'armadura ligera', 3, 2, 2, NULL, NULL, NULL),
(96, 'armadura pesada', 6, 2, 2, NULL, NULL, NULL),
(97, 'escudo(3)', 3, 2, 2, NULL, NULL, NULL),
(98, 'jabali', 24, 2, 3, NULL, NULL, NULL),
(99, 'serpiente alada', 200, 2, 3, NULL, NULL, NULL),
(100, 'karro de jabalies', 80, 2, 3, NULL, NULL, NULL),
(101, 'lobo gigante', 18, 2, 3, NULL, NULL, NULL),
(102, 'araña gigantesca', 40, 2, 3, NULL, NULL, NULL),
(103, 'karro de lobos', 60, 2, 3, NULL, NULL, NULL),
(104, 'garrapato cavernicola', 50, 2, 3, NULL, NULL, NULL),
(105, 'estandarte de batalla', 25, 2, 7, NULL, NULL, NULL),
(106, 'armadura ligera(2)', 2, 2, 2, NULL, NULL, NULL),
(107, 'jabali(16)', 16, 2, 3, NULL, NULL, NULL),
(108, 'armadura pesada(4)', 4, 2, 2, NULL, NULL, NULL),
(109, 'lobo gigante(12)', 12, 2, 3, NULL, NULL, NULL),
(110, 'rebadanora adicional(2)', 2, 2, 1, NULL, NULL, NULL),
(111, 'lanza(1)', 1, NULL, 1, NULL, NULL, NULL),
(112, 'grandotez', 4, 2, 13, NULL, NULL, NULL),
(113, 'estandarte magico', 50, 2, 13, NULL, NULL, NULL),
(114, 'arco(2)', 2, NULL, 1, NULL, NULL, NULL),
(115, 'arco corto(1)', 1, NULL, 1, NULL, NULL, NULL),
(116, 'arco corto(0)', 0, NULL, 1, NULL, NULL, NULL),
(117, 'fanaticos', 25, 2, 1, NULL, NULL, NULL),
(118, 'redes', 35, 2, 1, NULL, NULL, NULL),
(119, 'lanza(2)', 2, NULL, 1, NULL, NULL, NULL),
(120, 'tripulante adicional(5)', 5, NULL, 3, NULL, NULL, NULL),
(121, 'tripulante adicional(3)', 3, NULL, 3, NULL, NULL, NULL),
(122, 'lobo gigante adicional', 3, 2, 3, NULL, NULL, NULL),
(123, 'ezpabilagoblins', 5, 2, 3, NULL, NULL, NULL),
(124, 'troll piedra o troll rio', 20, 2, 13, NULL, NULL, NULL),
(125, 'espada veloz', 15, 2, 10, NULL, NULL, NULL),
(126, 'espada de batalla ', 15, 2, 10, NULL, NULL, NULL),
(127, 'espada del poder', 10, 2, 10, NULL, NULL, NULL),
(128, 'mordisco de acero', 5, 2, 10, NULL, NULL, NULL),
(129, 'escudo hechizado', 15, 2, 9, NULL, NULL, NULL),
(130, 'talisman de proteccion', 15, 2, 8, NULL, NULL, NULL),
(131, 'pergamino de dispersion', 25, 2, 6, NULL, NULL, NULL),
(132, 'piedra de energia', 25, 2, 6, NULL, NULL, NULL),
(133, 'baculo de hechicero', 30, 2, 6, NULL, NULL, NULL),
(134, 'estandarte de guerra', 25, 2, 7, NULL, NULL, NULL),
(135, 'kuchilla del waaaagh', 100, 2, 10, 'o', NULL, NULL),
(136, 'zuperrebanadora del ultimo waaagh', 100, 2, 10, 'o', NULL, NULL),
(137, 'hacha zangrienta de basha', 50, 2, 10, 'o', NULL, 1),
(138, 'ezpada chillona de shaga', 50, 2, 10, NULL, NULL, NULL),
(139, 'bakullo del kraneo de kaloth', 40, 2, 10, NULL, 1, NULL),
(140, 'pinchapuerkoz de porko', 40, 2, 10, NULL, NULL, 0),
(141, 'hacha zertera de ulag', 25, 2, 10, NULL, NULL, NULL),
(142, 'ezpada traizionera', 25, 2, 10, NULL, NULL, NULL),
(143, 'uniko kaztanazo de woolopa', 15, 2, 10, 'g', NULL, NULL),
(144, 'zuperhacha de martog', 15, 2, 10, NULL, NULL, NULL),
(145, 'zitepincho temato', 10, 2, 10, 'g', NULL, NULL),
(146, 'akuchillador zuertudo', 5, 2, 10, 'g', NULL, NULL),
(147, 'koraza de gorko', 50, 2, 9, NULL, NULL, NULL),
(148, 'ezkudo mordedor', 25, 2, 9, NULL, NULL, NULL),
(149, 'kareto de morko', 35, 2, 8, NULL, NULL, NULL),
(150, 'el mejor kachivache pal ke manda', 30, 2, 8, NULL, NULL, NULL),
(151, 'kolgante de protekzion', 25, 2, 8, NULL, NULL, NULL),
(152, 'el kollar de zorga', 5, 2, 8, NULL, NULL, NULL),
(153, 'idolo de morko', 50, 2, 6, 'o', NULL, NULL),
(154, 'bakulo chorizdor', 50, 2, 6, 'g', NULL, NULL),
(155, 'bakulo de baduumm', 40, 2, 6, NULL, NULL, NULL),
(156, 'garabatoz magikos', 10, 2, 6, 'o', NULL, NULL),
(157, 'zetaz magikas', 10, 2, 6, 'gn', NULL, NULL),
(158, 'kuerno de urgok', 40, 2, 5, NULL, NULL, NULL),
(159, 'garrapato gaita', 35, 2, 5, 'gn', NULL, NULL),
(160, 'javali lomoierro', 35, 2, 5, 'o', NULL, NULL),
(161, 'mandibula de hierro', 30, 2, 5, NULL, NULL, NULL),
(162, 'botaz pateakuloz de bigged', 30, 2, 5, NULL, NULL, NULL),
(163, 'mapa de maad', 25, 2, 5, NULL, NULL, 1),
(164, 'kachivache trukulento', 25, 2, 5, 'g', NULL, NULL),
(165, 'piedro renkorozo', 25, 2, 5, 'g', NULL, NULL),
(166, 'aro pegapinoz de nibbla', 20, 2, 5, NULL, NULL, NULL),
(167, 'zetaz zombreroloko', 20, 2, 5, 'gn', NULL, NULL),
(168, 'pozima tirapalante de guzzla', 15, 2, 5, NULL, NULL, NULL),
(169, 'totem de morko', 50, 2, 7, 'o', NULL, NULL),
(170, 'gran trapo rojo raido', 50, 2, 7, 'g', NULL, NULL),
(171, 'eztandarte de la luna malvada', 50, 2, 7, 'gn', NULL, NULL),
(172, 'eztandarte de la arana', 50, 2, 7, 'g', NULL, NULL),
(173, 'eztandarte del waaagh de gorko', 25, 2, 7, NULL, NULL, NULL),
(174, 'eztandarte del beztia de nogg', 25, 2, 7, NULL, NULL, NULL),
(175, 'arma de mano adicional(8)', 8, NULL, 1, NULL, NULL, NULL),
(176, 'mayal(8)', 8, NULL, 1, NULL, NULL, NULL),
(177, 'alabarda(8)', 8, 3, 1, NULL, NULL, NULL),
(178, 'escudo(10)', 10, 3, 2, NULL, NULL, NULL),
(179, 'marca de khorne(15)', 15, 3, 13, NULL, NULL, NULL),
(180, 'marca de nurgle(20)', 20, 3, 13, NULL, NULL, NULL),
(181, 'marca de tzeentch(10)', 10, 3, 13, NULL, NULL, NULL),
(182, 'marca de slaanesh(5)', 5, 3, 13, NULL, NULL, NULL),
(183, 'regalos del caos(50)', 50, 3, 13, NULL, NULL, NULL),
(184, 'dragon del caos', 360, 3, 3, NULL, NULL, NULL),
(185, 'corcel del caos con barda', 24, 3, 3, NULL, NULL, NULL),
(186, 'manticora', 200, 3, 3, NULL, NULL, NULL),
(187, 'montura demoniaca', 50, 3, 3, NULL, NULL, NULL),
(188, 'carro', 100, 3, 3, NULL, NULL, NULL),
(189, 'disco de tzeentch', 20, 3, 3, NULL, NULL, NULL),
(190, 'juggernaut de khorne', 50, 3, 3, NULL, NULL, NULL),
(191, 'palanquin de nurgle', 50, 3, 3, NULL, NULL, NULL),
(192, 'corcel de slaanesh', 25, 3, 3, NULL, NULL, NULL),
(193, 'marca de tzeentch(20)', 20, 3, 13, NULL, NULL, NULL),
(194, 'marca de khorne(10)', 10, 3, 13, NULL, NULL, NULL),
(195, 'marca de nurgle(30)', 30, 3, 13, NULL, NULL, NULL),
(196, 'marca de tzeentch(15)', 15, 3, 13, NULL, NULL, NULL),
(197, 'regalos del caos(100)', 100, 3, 13, NULL, NULL, NULL),
(198, 'regalos del caos(25)', 25, 3, 13, NULL, NULL, NULL),
(199, 'corcel del caos con barda(16)', 16, 3, 3, NULL, NULL, NULL),
(200, 'arma a dos manos(8)', 8, 3, 1, NULL, NULL, NULL),
(201, 'arma de mano adicional(4)', 4, NULL, 1, NULL, NULL, NULL),
(202, 'mayal(4)', 4, 3, 1, NULL, NULL, NULL),
(203, 'alabarda(4)', 4, NULL, 1, NULL, NULL, NULL),
(204, 'marca de slaanesh(10)', 10, 3, 13, NULL, NULL, NULL),
(205, 'escudo(5)', 5, NULL, 2, NULL, NULL, NULL),
(206, 'estandarte de batalla', 25, 3, 13, NULL, NULL, NULL),
(207, 'aumental nv hehicerial un nivel', 40, 3, 13, NULL, NULL, NULL),
(208, 'marca de khorne(30)', 30, 3, 13, NULL, NULL, NULL),
(209, 'arma de mano adicional(1)', 1, NULL, 1, NULL, NULL, NULL),
(210, 'arma a dos manos(2)', 2, NULL, 1, NULL, NULL, NULL),
(211, 'alabarda(1)', 1, NULL, 1, NULL, NULL, NULL),
(212, 'mayal(1)', 1, NULL, 1, NULL, NULL, NULL),
(213, 'arma a dos manos(1)', 1, NULL, 1, NULL, NULL, NULL),
(214, 'ataques envenenados(3)', 3, NULL, 1, NULL, NULL, NULL),
(215, 'piel escamosa(6)(1)', 1, NULL, 2, NULL, NULL, NULL),
(216, 'marcha de tzeentch(20)', 20, 3, 13, NULL, NULL, NULL),
(217, 'mayal(2)', 2, NULL, 1, NULL, NULL, NULL),
(218, 'lanzas arrojadizas(1)', 1, NULL, 1, NULL, NULL, NULL),
(219, 'hachas arrojadizas(2)', 2, NULL, 1, NULL, NULL, NULL),
(220, 'armadura del caos(5)', 5, 3, 2, NULL, NULL, NULL),
(221, 'arma de mano adicional(5)', 5, NULL, 1, NULL, NULL, NULL),
(222, 'arma a dos manos(10)', 10, NULL, 1, NULL, NULL, NULL),
(223, 'arma a dos manos(12)', 12, NULL, 1, NULL, NULL, NULL),
(224, 'lanza de caballeria(5)', 5, NULL, 1, NULL, NULL, 0),
(225, 'marca de khorne(20)', 20, 3, 13, NULL, NULL, NULL),
(226, 'marca de slaanesh(20)', 20, 3, 13, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miniaturas`
--

CREATE TABLE `miniaturas` (
  `id` int(11) NOT NULL,
  `maximo` int(11) DEFAULT NULL,
  `minimo` int(11) DEFAULT NULL,
  `puntos` int(11) NOT NULL,
  `idregimiento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `miniaturas`
--

INSERT INTO `miniaturas` (`id`, `maximo`, `minimo`, `puntos`, `idregimiento`) VALUES
(1, NULL, 10, 11, 12),
(2, NULL, 10, 9, 13),
(3, NULL, 10, 12, 14),
(4, NULL, 5, 15, 15),
(5, NULL, 5, 15, 16),
(6, NULL, 5, 15, 17),
(7, NULL, 5, 21, 18),
(8, NULL, 5, 30, 19),
(9, NULL, 5, 17, 20),
(10, NULL, 5, 16, 21),
(11, NULL, 10, 5, 49),
(12, NULL, 10, 6, 50),
(13, NULL, 10, 8, 51),
(14, NULL, 20, 3, 52),
(15, NULL, 5, 12, 53),
(16, NULL, 5, 12, 54),
(17, NULL, 20, 3, 55),
(18, 20, 2, 20, 56),
(19, NULL, 5, 13, 57),
(20, NULL, 5, 22, 58),
(21, NULL, 5, 21, 59),
(22, 1, 1, 80, 60),
(23, 10, 5, 15, 61),
(24, 1, 1, 60, 63),
(25, 2, 1, 35, 65),
(26, 1, 1, 70, 64),
(27, NULL, 1, 40, 66),
(28, 2, 1, 40, 68),
(29, NULL, 10, 15, 84),
(30, NULL, 10, 4, 85),
(31, NULL, 5, 6, 86),
(32, NULL, 5, 13, 87),
(33, NULL, 5, 18, 88),
(34, NULL, 5, 18, 89),
(35, NULL, 3, 35, 91),
(36, NULL, 3, 65, 92),
(37, NULL, 3, 45, 93),
(38, NULL, 5, 40, 94),
(39, 2, 1, 55, 95);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `musico`
--

CREATE TABLE `musico` (
  `id` int(11) NOT NULL,
  `puntos` int(11) NOT NULL,
  `idregimiento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `musico`
--

INSERT INTO `musico` (`id`, `puntos`, `idregimiento`) VALUES
(1, 5, 12),
(2, 5, 13),
(3, 5, 14),
(4, 6, 15),
(5, 6, 16),
(6, 6, 17),
(7, 8, 18),
(8, 10, 19),
(9, 7, 20),
(10, 5, 49),
(11, 5, 50),
(12, 5, 51),
(13, 4, 52),
(14, 6, 53),
(15, 6, 54),
(16, 4, 55),
(17, 6, 57),
(18, 7, 58),
(18, 7, 59),
(20, 6, 84),
(21, 4, 85),
(22, 6, 87),
(23, 10, 88),
(24, 10, 91),
(25, 10, 94);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portaestandarte`
--

CREATE TABLE `portaestandarte` (
  `id` int(11) NOT NULL,
  `puntos` int(11) NOT NULL,
  `puntosequipo` int(11) DEFAULT NULL,
  `idregimiento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `portaestandarte`
--

INSERT INTO `portaestandarte` (`id`, `puntos`, `puntosequipo`, `idregimiento`) VALUES
(1, 10, NULL, 12),
(2, 10, NULL, 13),
(3, 10, NULL, 14),
(4, 12, 50, 15),
(5, 12, 50, 16),
(6, 12, 50, 17),
(7, 16, NULL, 18),
(8, 20, 50, 19),
(9, 14, NULL, 20),
(10, 10, NULL, 49),
(11, 10, NULL, 50),
(12, 10, NULL, 51),
(13, 8, NULL, 52),
(14, 12, NULL, 53),
(15, 12, NULL, 54),
(16, 8, NULL, 55),
(17, 12, 50, 57),
(18, 14, 50, 58),
(18, 14, 50, 59),
(20, 23, 50, 84),
(21, 8, NULL, 85),
(22, 12, NULL, 87),
(23, 20, 50, 88),
(24, 20, NULL, 91),
(25, 20, 50, 94);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regimientos`
--

CREATE TABLE `regimientos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `puntos` int(11) DEFAULT NULL,
  `objetos_magicos` int(11) DEFAULT NULL,
  `idejercito` int(11) DEFAULT NULL,
  `tipo` varchar(100) DEFAULT NULL,
  `pie` tinyint(1) DEFAULT NULL,
  `mago` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `regimientos`
--

INSERT INTO `regimientos` (`id`, `nombre`, `idunidad`, `puntos`, `objetos_magicos`, `idejercito`, `tipo`, `pie`, `mago`) VALUES
(1, 'tyrion', 4, 400, NULL, 1, NULL, NULL, NULL),
(2, 'teclis', 4, 475, NULL, 1, NULL, NULL, NULL),
(3, 'eltharion', 4, 285, NULL, 1, NULL, NULL, NULL),
(4, 'alith anar', 4, 245, NULL, 1, NULL, NULL, NULL),
(5, 'principe', 4, 150, 100, 1, NULL, 2, NULL),
(6, 'archimago', 4, 225, 100, 1, NULL, 2, 1),
(7, 'caradryan', 5, 175, NULL, 1, NULL, NULL, NULL),
(8, 'korhil', 5, 140, NULL, 1, NULL, NULL, NULL),
(9, 'noble', 5, 85, 50, 1, NULL, 2, NULL),
(10, 'mago', 5, 100, 50, 1, NULL, 2, 1),
(11, 'mago dragon de caledor', 5, 350, 50, 1, NULL, 0, 1),
(12, 'arqueros', 1, NULL, NULL, 1, NULL, 1, NULL),
(13, 'lanceros', 1, NULL, NULL, 1, NULL, 1, NULL),
(14, 'guardia del mar de lothern', 1, NULL, NULL, 1, NULL, 1, NULL),
(15, 'maestros de la espada', 2, NULL, NULL, 1, NULL, 1, NULL),
(16, 'guardia del fenix', 2, NULL, NULL, 1, NULL, 1, NULL),
(17, 'leones blancos', 2, NULL, NULL, 1, NULL, 1, NULL),
(18, 'yelmos plateados', 2, NULL, NULL, 1, NULL, 0, NULL),
(19, 'principes dragon', 2, NULL, NULL, 1, NULL, 0, NULL),
(20, 'guardianes de ellyrion', 2, NULL, NULL, 1, NULL, 0, NULL),
(21, 'sombrios', 2, NULL, NULL, 1, NULL, 1, NULL),
(22, 'carro de tiranoc', 2, 85, NULL, 1, NULL, 0, NULL),
(23, 'carros de leones blancos', 2, 140, NULL, 1, NULL, 0, NULL),
(24, 'lanzavirotes', 3, 100, NULL, 1, NULL, NULL, NULL),
(25, 'aguila gigante', 3, 50, NULL, 1, NULL, NULL, NULL),
(26, 'gorbad garraierro', 9, 310, NULL, 2, NULL, NULL, NULL),
(27, 'azhag el carnicero', 9, 450, NULL, 2, NULL, NULL, NULL),
(28, 'grimgor pielierro', 9, 375, NULL, 2, NULL, NULL, NULL),
(29, 'grom el panzudo', 9, 255, NULL, 2, NULL, NULL, NULL),
(30, 'skarsnik senor ocho picos', 9, 205, NULL, 2, NULL, NULL, NULL),
(31, 'kaudillo orco', 9, 120, 100, 2, 'o', 2, 0),
(32, 'kaudillo orco zalvaje', 9, 125, 100, 2, 'o', 2, 0),
(33, 'kaudillo orco negro', 9, 145, 100, 2, 'o', 2, 0),
(34, 'gran chaman orco', 9, 180, 100, 2, 'o', 2, 1),
(35, 'gran chaman orco zalvaje', 9, 185, 100, 2, 'o', 2, 1),
(36, 'kaudillo goblin', 9, 65, 100, 2, 'g', 2, NULL),
(37, 'kaudillo goblin nocturno', 9, 55, 100, 2, 'g', 2, NULL),
(38, 'gran chaman goblin', 9, 155, 100, 2, 'g', 2, NULL),
(39, 'gran chaman goblin nocturno', 9, 150, 100, 2, 'g', 2, NULL),
(40, 'gran jefe orco', 10, 70, 50, 2, 'o', 2, NULL),
(41, 'gran jefe orco zalvaje', 10, 75, 50, 2, 'o', 2, NULL),
(42, 'gran jefe orco negro', 10, 85, 50, 2, 'o', 2, NULL),
(43, 'chaman orco', 10, 65, 50, 2, 'o', 2, 1),
(44, 'chaman orco zalvaje', 10, 70, 50, 2, 'o', 2, 1),
(45, 'gran jefe goblin', 10, 35, 50, 2, 'g', 2, NULL),
(46, 'gran jefe goblin nocturno', 10, 30, 50, 2, 'g', 2, NULL),
(47, 'chaman goblin', 10, 55, 50, 2, 'g', 2, 1),
(48, 'chaman goblin nocturno', 10, 50, 50, 2, 'g', 2, 1),
(49, 'gerreroz orcos', 6, NULL, NULL, 2, 'o', 1, NULL),
(50, 'arkeroz orcos', 6, NULL, NULL, 2, 'o', 1, NULL),
(51, 'orcos zalvajes', 6, NULL, NULL, 2, 'o', 1, NULL),
(52, 'goblins', 6, NULL, NULL, 2, 'g', 1, NULL),
(53, 'jinetez de lobo goblins', 6, NULL, NULL, 2, 'g', 0, NULL),
(54, 'jenetez de araña goblins', 6, NULL, NULL, 2, 'g', 0, NULL),
(55, 'goblins nocturnos', 6, NULL, NULL, 2, 'g', 1, NULL),
(56, 'snotlings', 6, NULL, NULL, 2, NULL, 1, NULL),
(57, 'orcos negroz', 7, NULL, NULL, 2, 'o', 1, NULL),
(58, 'jinetez jabali orcos', 7, NULL, NULL, 2, 'o', 0, NULL),
(59, 'jinetes jabali orcos zalvajes', 7, NULL, NULL, 2, 'o', 0, NULL),
(60, 'karro de jabalies orco', 7, NULL, NULL, 2, 'o', 0, NULL),
(61, 'garrapatos saltarines', 7, NULL, NULL, 2, NULL, 0, NULL),
(62, 'pastores garrapatos', 7, 30, NULL, 2, NULL, 1, NULL),
(63, 'karro de lobos goblin', 7, NULL, NULL, 2, 'g', 0, NULL),
(64, 'lanzapiedroz', 7, NULL, NULL, 2, NULL, NULL, NULL),
(65, 'lanzapinchoz', 7, NULL, NULL, 2, NULL, NULL, NULL),
(66, 'trolls', 8, NULL, NULL, 2, NULL, 1, NULL),
(67, 'katapulta goblins voladores', 8, 80, NULL, 2, NULL, NULL, NULL),
(68, 'vagoneta snotling', 8, NULL, NULL, 2, NULL, 0, NULL),
(69, 'gigante', 8, 205, NULL, 2, NULL, 1, NULL),
(70, 'archaon', 11, 685, NULL, 3, NULL, NULL, NULL),
(71, 'galrauch', 11, 616, NULL, 3, NULL, NULL, NULL),
(72, 'principe sigvald el magnifico', 11, 425, NULL, 3, NULL, NULL, NULL),
(73, 'kholek suneater', 11, 605, NULL, 3, NULL, NULL, NULL),
(74, 'valkia la sanguinaria', 11, 410, NULL, 3, NULL, NULL, NULL),
(75, 'vilitch el maldito', 11, 395, NULL, 3, NULL, NULL, NULL),
(76, 'señor del caos', 11, 395, 100, 3, NULL, 2, 0),
(77, 'gran hechicero', 11, 235, 100, 3, NULL, 2, 1),
(78, 'principe demonio', 11, 300, NULL, 3, NULL, 1, 0),
(79, 'festus dr anelido', 12, 185, NULL, 3, NULL, NULL, NULL),
(80, 'rey troll throgg', 12, 175, NULL, 3, NULL, NULL, NULL),
(81, 'wulfrik el errante', 12, 185, NULL, 3, NULL, NULL, NULL),
(82, 'hechicero del caos', 12, 85, 50, 3, NULL, 2, 1),
(83, 'paladin del caos', 12, 110, 50, 3, NULL, 2, 0),
(84, 'gerreros del caos', 13, NULL, NULL, 3, NULL, 1, 0),
(85, 'barbaros del caos', 13, NULL, NULL, 3, NULL, 1, 0),
(86, 'mastines del caos', 13, NULL, NULL, 3, NULL, 1, 0),
(87, 'jinetes barbaros del caos', 13, NULL, NULL, 3, NULL, 0, 0),
(88, 'elegidos', 14, NULL, NULL, 3, NULL, 1, 0),
(89, 'malditos', 14, NULL, NULL, 3, NULL, 1, 0),
(90, 'carro del caos', 14, 120, NULL, 3, NULL, 0, 0),
(91, 'orgros', 14, NULL, NULL, 3, NULL, 1, 0),
(92, 'ogros dragon', 14, NULL, NULL, 3, NULL, 1, 0),
(93, 'trolls del caos', 14, NULL, NULL, 3, NULL, 1, 0),
(94, 'caballeros del caos', 14, NULL, NULL, 3, NULL, 0, 0),
(95, 'engendros del caos', 15, NULL, NULL, 3, NULL, 1, 0),
(96, 'scyla anfingrimm', 15, 105, NULL, 3, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_equipo`
--

CREATE TABLE `tipo_equipo` (
  `id` int(11) NOT NULL,
  `tipo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_equipo`
--

INSERT INTO `tipo_equipo` (`id`, `tipo`) VALUES
(1, 'armas'),
(2, 'armaduras'),
(3, 'montura'),
(4, 'eqiupo adicional'),
(5, 'objetos hechizados'),
(6, 'objetos arcanos'),
(7, 'estandartes magicos'),
(8, 'talismanes'),
(9, 'armaduras magicas'),
(10, 'armas magicas'),
(11, 'hechicero nv4'),
(12, 'hechicero nv2'),
(13, 'especiales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidades`
--

CREATE TABLE `unidades` (
  `id` int(11) NOT NULL,
  `categoria` varchar(100) NOT NULL,
  `maximo` int(11) NOT NULL,
  `puntospartida` int(11) NOT NULL,
  `idejercito` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidades`
--

INSERT INTO `unidades` (`id`, `categoria`, `maximo`, `puntospartida`, `idejercito`) VALUES
(1, 'basicas', 2, 2000, 1),
(2, 'especiales', 6, 2000, 1),
(3, 'singulares', 4, 2000, 1),
(4, 'comandantes', 4, 2000, 1),
(5, 'heroes', 1, 2000, 1),
(6, 'basicas', 2, 2000, 2),
(7, 'especiales', 4, 2000, 2),
(8, 'singulares', 2, 2000, 2),
(9, 'comandantes', 1, 2000, 2),
(10, 'heroes', 4, 2000, 2),
(11, 'comandantes', 1, 2000, 3),
(12, 'heroes', 4, 2000, 3),
(13, 'basicas', 3, 2000, 3),
(14, 'especiales', 4, 2000, 3),
(15, 'singulares', 2, 2000, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unionejercitoregimiento`
--

CREATE TABLE `unionejercitoregimiento` (
  `id` int(11) NOT NULL,
  `idejercito` int(11) NOT NULL,
  `idregimiento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unionejercitoregimiento`
--

INSERT INTO `unionejercitoregimiento` (`id`, `idejercito`, `idregimiento`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `campeon`
--
ALTER TABLE `campeon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idregimiento` (`idregimiento`);

--
-- Indices de la tabla `ejercito`
--
ALTER TABLE `ejercito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `equiporegimiento`
--
ALTER TABLE `equiporegimiento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idequipoun` (`idequipo`),
  ADD KEY `idreg` (`idregimiento`);

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_ejercito` (`idejercito`),
  ADD KEY `idcategoria` (`categoria_arma`);

--
-- Indices de la tabla `miniaturas`
--
ALTER TABLE `miniaturas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idregimientocuatro` (`idregimiento`);

--
-- Indices de la tabla `musico`
--
ALTER TABLE `musico`
  ADD KEY `idregimientodos` (`idregimiento`);

--
-- Indices de la tabla `portaestandarte`
--
ALTER TABLE `portaestandarte`
  ADD KEY `idregimientotres` (`idregimiento`);

--
-- Indices de la tabla `regimientos`
--
ALTER TABLE `regimientos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idunidad` (`idunidad`),
  ADD KEY `ideje` (`idejercito`);

--
-- Indices de la tabla `tipo_equipo`
--
ALTER TABLE `tipo_equipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `unidades`
--
ALTER TABLE `unidades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idejercito` (`idejercito`);

--
-- Indices de la tabla `unionejercitoregimiento`
--
ALTER TABLE `unionejercitoregimiento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idejercitopk` (`idejercito`),
  ADD KEY `idregimientopk` (`idregimiento`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `campeon`
--
ALTER TABLE `campeon`
  ADD CONSTRAINT `idregimiento` FOREIGN KEY (`idregimiento`) REFERENCES `regimientos` (`id`);

--
-- Filtros para la tabla `equiporegimiento`
--
ALTER TABLE `equiporegimiento`
  ADD CONSTRAINT `idequipoun` FOREIGN KEY (`idequipo`) REFERENCES `equipos` (`id`),
  ADD CONSTRAINT `idreg` FOREIGN KEY (`idregimiento`) REFERENCES `regimientos` (`id`);

--
-- Filtros para la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD CONSTRAINT `id_ejercito` FOREIGN KEY (`idejercito`) REFERENCES `ejercito` (`id`),
  ADD CONSTRAINT `idcategoria` FOREIGN KEY (`categoria_arma`) REFERENCES `tipo_equipo` (`id`);

--
-- Filtros para la tabla `miniaturas`
--
ALTER TABLE `miniaturas`
  ADD CONSTRAINT `idregimientocuatro` FOREIGN KEY (`idregimiento`) REFERENCES `regimientos` (`id`);

--
-- Filtros para la tabla `musico`
--
ALTER TABLE `musico`
  ADD CONSTRAINT `idregimientodos` FOREIGN KEY (`idregimiento`) REFERENCES `regimientos` (`id`);

--
-- Filtros para la tabla `portaestandarte`
--
ALTER TABLE `portaestandarte`
  ADD CONSTRAINT `idregimientotres` FOREIGN KEY (`idregimiento`) REFERENCES `regimientos` (`id`);

--
-- Filtros para la tabla `regimientos`
--
ALTER TABLE `regimientos`
  ADD CONSTRAINT `ideje` FOREIGN KEY (`idejercito`) REFERENCES `ejercito` (`id`),
  ADD CONSTRAINT `idunidad` FOREIGN KEY (`idunidad`) REFERENCES `unidades` (`id`);

--
-- Filtros para la tabla `unidades`
--
ALTER TABLE `unidades`
  ADD CONSTRAINT `idejercito` FOREIGN KEY (`idejercito`) REFERENCES `ejercito` (`id`);

--
-- Filtros para la tabla `unionejercitoregimiento`
--
ALTER TABLE `unionejercitoregimiento`
  ADD CONSTRAINT `idejercitopk` FOREIGN KEY (`idejercito`) REFERENCES `ejercito` (`id`),
  ADD CONSTRAINT `idregimientopk` FOREIGN KEY (`idregimiento`) REFERENCES `regimientos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
